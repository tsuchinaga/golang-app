package main

import (
	"github.com/maxence-charriere/app"
	"log"
)

type Hello struct {
	Name string
}

func (h *Hello) Render() (r string) {
	r = `
<div class="Hello">
    <h1>
        Hello
        {{if .Name}}
            {{.Name}}
        {{else}}
            world
        {{end}}!
    </h1>
    <input value="{{.Name}}" placeholder="What is your name?" onchange="{{bind "Name"}}" autofocus>
</div>
`
	return
}

func main() {
	app.Import(&Hello{})

	app.DefaultPath = "/hello"

	if err := app.Run(); err != nil {
		log.Println(err)
	}
}
