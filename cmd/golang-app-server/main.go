package main

import (
	"github.com/maxence-charriere/app"
	"log"
	"net/http"
)

func main() {
	http.Handle("/", &app.Handler{})

	if err := http.ListenAndServe(":3000", nil); err != nil {
		log.Fatalln(err)
	}
}
