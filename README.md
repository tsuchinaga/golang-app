# golang-app

[maxence-charriere/app](https://github.com/maxence-charriere/app)を利用して、Webページを作ってみる。

結果的にSPAのようなページになると思われる

## 手順

1. `goapp`関連のインストール
    ```bash
    $ go get -u github.com/maxence-charriere/app
    $ go get -u github.com/maxence-charriere/app/cmd/goapp
    ```
2. 初期化
    ```bash
    $ goapp init
    ```
3. アプリケーションの実装(`./cmd/golang-app-wasm/main.go`)
4. サーバの実装(`./cmd/golang-app-server/main.go`)
5. 実行
    ```bash
    $ goapp run
    ```
    * このとき` open E:\Projects\go\src\github.com\maxence-charriere\app\logo.png: The system cannot find the path specified.`というようなエラーがでるなら、`./web/icon.png`を設置してください。
    * `web/icon.png`の例は、公式の`app/logo.png`です

