module gitlab.com/tsuchinaga/golang-app

go 1.12

require (
	github.com/disintegration/imaging v1.6.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/maxence-charriere/app v0.0.0-20190326183908-311004d5e5bf
	github.com/pkg/errors v0.8.1 // indirect
	github.com/segmentio/conf v1.1.0 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
)
